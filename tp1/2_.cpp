#include <iostream>
#include <type_traits>



template<typename T>
struct volume { static int const value = 1; };

template<class T, std::size_t N>
struct volume<T[N]> { static size_t const value = volume<T>::value * N; };



template<int I>
struct fibonacci { static constexpr size_t value = fibonacci<I-2>::value + fibonacci<I-1>::value; };

template<>
struct fibonacci<0> { static constexpr int value = 0; };

template<>
struct fibonacci<1> { static constexpr size_t value = 1; };



template<int I, int J>
struct euclidian_division
{
  static constexpr int quotient = I/J;
  static constexpr int remainder = I%J;
};



int main()
{
  std::cout<<"volume<void> = "<< volume<void>::value <<std::endl;
  std::cout<<"volume<int> = "<< volume<int>::value <<std::endl;
  std::cout<<"volume<int[]> = "<< volume<int[]>::value <<std::endl;
  std::cout<<"volume<int[2]> = "<< volume<int[2]>::value <<std::endl;
  std::cout<<"volume<int[2][4]> = "<< volume<int[2][4]>::value <<std::endl;
  std::cout<<"volume<int[2][3][4]> = "<< volume<int[2][3][4]>::value <<std::endl;
  std::cout<<"volume<int[2][3][4][1]> = "<< volume<int[2][3][4][1]>::value <<std::endl;
  std::cout<<"volume<int[2][3][4][2]> = "<< volume<int[2][3][4][2]>::value <<std::endl;
  std::cout<<std::endl;

  std::cout<<"fibonacci<0> = "<< fibonacci<0>::value <<std::endl;
  std::cout<<"fibonacci<1> = "<< fibonacci<1>::value <<std::endl;
  std::cout<<"fibonacci<2> = "<< fibonacci<2>::value <<std::endl;
  std::cout<<"fibonacci<3> = "<< fibonacci<3>::value <<std::endl;
  std::cout<<"fibonacci<4> = "<< fibonacci<4>::value <<std::endl;
  std::cout<<"fibonacci<5> = "<< fibonacci<5>::value <<std::endl;
  std::cout<<"fibonacci<6> = "<< fibonacci<6>::value <<std::endl;
  std::cout<<"fibonacci<16> = "<< fibonacci<16>::value <<std::endl;
  std::cout<<std::endl;

  std::cout<<"euclidian_division<17,3>::quotient = "<<euclidian_division<17,3>::quotient<<std::endl;
  std::cout<<"euclidian_division<17,3>::remainder = "<<euclidian_division<17,3>::remainder<<std::endl;
  std::cout<<"euclidian_division<5,6>::quotient = "<<euclidian_division<5,6>::quotient<<std::endl;
  std::cout<<"euclidian_division<5,6>::remainder = "<<euclidian_division<5,6>::remainder<<std::endl;
  std::cout<<"euclidian_division<6,6>::quotient = "<<euclidian_division<6,6>::quotient<<std::endl;
  std::cout<<"euclidian_division<6,6>::remainder = "<<euclidian_division<6,6>::remainder<<std::endl;

  return 0;
}
