#include <iostream>
#include <type_traits>
#include <array>
#include <vector>
#include <functional>



//// 1 is_void
template <typename T>
struct is_void : public std::false_type
{
  //static constexpr bool value = false; //replaced by false_type
};

template<>
struct is_void<void> : public std::true_type
{
  //static constexpr bool value = true; //replaced by true_type
};

//// 2 is_same
template<typename, typename>
struct is_same : public std::false_type
{};

template<typename T>
struct is_same<T, T> : public std::true_type
{};

//// 3 is_same

/*
template <typename T, typename U>
using subscript = decltype(std::declval<T>()[std::declval<U>()]);

template<typename T, typename U>
struct has_subscript<T, subscript<T,U>> : public std::true_type
{};

template<typename, typename>
struct has_subscript : public std::false_type
{};
*/ //marche pas car pas plus spécialisé que l'autre ...

// has_subscript
template<typename T, typename U>
struct has_subscript
{
  template<typename, typename>
  static auto test(...) -> std::false_type;

  template<typename A, typename B>
  static auto test(int) -> decltype((std::declval<A>()[std::declval<B>()]), std::true_type());

  static constexpr bool value = is_same<decltype(test<T,U>(0)), std::true_type>();
};


template<typename T, typename... A>
struct is_invocable
{
  template<typename, typename...>
  static auto test(...) -> std::false_type;

  template<typename F, typename... P>
  static auto test(int) -> decltype(std::declval<F>()(std::declval<P>()...), std::true_type());

  static constexpr bool value = is_same<decltype(test<T,A...>(0)), std::true_type>();
};

int add(int a, int b){ return a+b; }

int main()
{
  //// 1 is_void
  std::cout<<"\tTP1 metaprog\n"<<std::endl;
  std::cout<<"is_void<int> = "<<is_void<int>::value<<std::endl;
  std::cout<<"is_void<void> = "<<is_void<void>::value<<std::endl;
  std::cout<<std::endl;

  //// 2 is_same
  std::cout<<"is_same<int, double> = "<<is_same<int, double>::value<<std::endl;
  std::cout<<"is_same<int, int> = "<<is_same<int, int>::value<<std::endl;
  std::cout<<std::endl;

  //// 3 has_subscript
  std::cout<<"has_subscript<int, double> = "<<has_subscript<int, double>::value<<std::endl;
  std::cout<<"has_subscript<int[], int> = "<<has_subscript<int[], int>::value<<std::endl;
  std::cout<<std::endl;

  //// 4 is_invocable
  std::function<int (int)> func = [](int i) { return i+4; };
  std::cout<<"is_invocable<add, int, int> = "<<is_invocable<decltype(add), int, int>::value<<std::endl;
  std::cout<<"is_invocable<add, int> = "<<is_invocable<decltype(add), int>::value<<std::endl;
  std::cout<<std::endl;

  return 0;
}
