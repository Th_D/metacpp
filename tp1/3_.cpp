#include <iostream>
#include <array>
#include <list>
#include <vector>

//default template
template<class T, class U = void>
struct is_container : public std::false_type
{};

//just to use list of types as param
template<class... Ts>
struct liste_type
{};

//contains all elements of a container (except constructor and [] (should use has_subscript))
template<class T>
using container_carac = liste_type
<
  decltype(std::declval<T>().begin()),
  decltype(std::declval<T>().end()),
  decltype(std::declval<T>().cbegin()),
  decltype(std::declval<T>().cend()),
  decltype(std::declval<T>().size()),
  decltype(std::declval<T>().max_size()),
  decltype(std::declval<T>().empty()),
  typename T::value_type,
  typename T::reference,
  typename T::const_reference,
  typename T::iterator,
  typename T::const_iterator,
  typename T::difference_type,
  typename T::size_type
>;

//template plus spécialisé
template<class T>
struct is_container<T, std::conditional_t<false, container_carac<T>, void>>
: public std::true_type
{};

int main()
{
  std::cout<<"is_container<void> = "<<is_container<void>::value<<std::endl;
  std::cout<<"is_container<int> = "<<is_container<int>::value<<std::endl;
  std::cout<<"is_container<int[2]> = "<<is_container<int[2]>::value<<std::endl;
  std::cout<<"is_container<string> = "<<is_container<std::string>::value<<std::endl;
  std::cout<<"is_container<std::array<int, 2>> = "<<is_container<std::array<int, 2>>::value<<std::endl;
  std::cout<<"is_container<std::vector<std::array<int, 3>>> = "<<is_container<std::vector<std::array<int, 3>>>::value<<std::endl;



	return 0;
}
