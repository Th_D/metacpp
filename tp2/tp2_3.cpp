/*
  Thibault Delorme, 20191210
  metaprogramming tp 2 part. 3
  g++ tp2_3.cpp -std=c++17 -o exe && ./exe
*/

#include <iostream>

/*Q3.1 – Ecrivez une structure gray_pixel qui encapsule un entier 8 bits non signé représentant un
niveau de gris dans une image, 0 étant le noir, 255 le gris.*/

struct gray_pixel
{
  //grey level (0=black, 255=white)
  unsigned char grey;

  int intensity(){return grey-127;}
};

/*Q3.2 – Ecrivez une structure rgb_pixel qui encapsule 3 flottants simple précision dont la valeur
varie de 0 à 1 et représente un pixel Rouge/Vert/Bleu. (0,0,0) représente le noir, (1,1,1) représente le
blanc*/

struct rgb_pixel
{
  //rgb code c[0,0,0(black) ; 1,1,1(white)]
  //tuple?
  float r;
  float g;
  float b;

  int intensity(){return (((r+g+b)/3*255)-127);}
};


/*Q3.3 – Ecrivez une fonction intensity qui calcule l’intensité lumineuse d’un pixel. Pour les pixels
en niveau de gris, il s’agit de la valeur normalisée entre -127 (le noir) et 128(le blanc). Pour les pixels
RGB, il s’agit de la moyenne entre les valeurs des trois composantes renormalisées entre -127 et 128.
Utiliser la programmation générique pour factoriser au mieux cette fonction.*/

template <typename PIX>
int intensity(PIX pixel) //on peut aussi faire une fonction par struct pour overloader, mais les fontion de struct c'est plutot approprié ici
{
  return pixel.intensity();
}

/*Q3.4 – Ecrivez une fonction average_intensity prenant une séquence quelconque de pixel de
type quelconque et qui renvoie son intensité moyenne. Testez votre code pour différents types de
séquences et différents types de pixels.*/

/* JUST SUM wth recursion
int average_intensity()
{
  return 0;
}

template <typename PIX, typename... PIXS>
int average_intensity(PIX p, PIXS ...pixs)
{
  return (intensity(p)+average_intensity(pixs...));
}*/

template <typename... PIXS>
int average_intensity(PIXS... pixels)
{
  int nb = ((pixels, 1)+...); //WAAA
  int sum = (intensity(pixels)+...);
  return (sum/nb);
}

int main()
{
  //Q1
  gray_pixel g_pix = {100};

  //Q2
  rgb_pixel rgb_pix = {0.2, 1, 0};
  rgb_pixel rgb_pix2 = {0.2, 1, 1};

  //Q3
  std::cout<<"intensity(grey_pix) = "<<intensity(g_pix)<<std::endl;
  std::cout<<"intensity(rgb_pix) = "<<intensity(rgb_pix)<<std::endl;
  std::cout<<"intensity(rgb_pix2) = "<<intensity(rgb_pix2)<<std::endl;

  //Q4
  std::cout<<"intensity(grey_pix, rgb_pix, rgb_pix2) = "<<average_intensity(g_pix, rgb_pix, rgb_pix2)<<std::endl;

  return 0;

}
