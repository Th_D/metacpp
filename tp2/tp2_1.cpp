/*
  Thibault Delorme, 20191210
  metaprogramming tp 2 part. 1
  g++ tp2_1.cpp -std=c++17 -o exe && ./exe
*/

#include <iostream>
#include <string>
#include <utility>
#include <tuple>
#include <type_traits>


template<int I>
using int_ = std::integral_constant<int, I>;


template<int S, typename Code, int... Idx>
void for_constexpr(Code c, std::integer_sequence<int, Idx...> const&, int_<S> const&)
{
  bool dummy[] = { (c(int_<S + Idx>{}), true)... };
}


template<int S, int E, typename Code>
void for_constexpr(Code c)
{
  for_constexpr(c, std::make_integer_sequence<int, E - S>{}, int_<S>{});
}

/*
Q1.
le code compile et affiche:
"
*
**
***
****
"

Q2.
for_constexpr:
comme son nom l'indique,
permet de faire des sortes de boucles for pas très belles à la compilation
*/

//Q3.
template<int N, int M>
void fill_array(std::array<std::array<double,N>,M>& A)
{
  for_constexpr<0, M>([&](auto i) {
    for_constexpr<0, N>([&](auto j) {
      A[i][j] = 1000*(i+1) + (j+1);
    });
  });
}

//Q4.
template<int N, int M>
void disp_array(std::array<std::array<double,N>,M>& A)
{
  for_constexpr<0, M>([&](auto i) {
    for_constexpr<0, N>([&](auto j) {
      std::cout<<A[i][j]<<" ";
    });
    std::cout<<'\n';
  });
}

int main()
{
  for_constexpr<1, 5>([&](auto i) { std::cout << std::string(i, '*') << "\n"; });

  std::array<std::array<double,3>,2> A = {1,2,3,4,5,6};


  /*for_constexpr<1,5>([](int i)
  {
  std::cout<<i;
});*/

fill_array<3,2>(A);
disp_array<3,2>(A);

}
