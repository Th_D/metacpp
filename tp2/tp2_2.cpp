/*
  Thibault Delorme, 20191210
  metaprogramming tp 2 part. 2
  g++ tp2_2.cpp -std=c++17 -o exe && ./exe
*/

#include <iostream>
#include <array>
#include <cstring>

/*Q2.1 – Ecrivez une méta-fonction qui, à partir d’un type T calcule le type d’un tableau d’entier 8 bits
non-signés de taille suffisante à contenir un élément de type T.*/

template <typename T>
using equintarr = std::array<unsigned short, sizeof(T)/sizeof(short)+(sizeof(T)%sizeof(short)==0?0:1)>;

/*Q2.2 – Ecrivez une fonction serialize qui prend en paramètre un type T et recopie son contenu
dans un tableau de taille adéquate*/

template<typename T>
equintarr<T> serialize(T arg)
{
  equintarr<T> res = {};
  memcpy(res, arg, sizeof(arg));
  return res;
}

/*Q2.3 – Ecrivez une fonction deserialize qui prend un tableau d’octet de taille arbitraire et
recopie son contenu dans une élément de type T. Quel mécanisme pouvez-vous utiliser pour vérifier
que la taille du type de destination est compatible avec la taille du tableau ?*/

template<typename T>
T deserialize(equintarr<T> arg){
	T res;
	std::memcpy(&res, &arg, sizeof(T));
	return res;
}

struct _3{
  bool b;bool b2;bool b3;
};

struct _9{
  bool b1;bool b2;bool b3;
  bool b4;bool b5;bool b6;
  bool b9;bool b8;bool b7;
};

int main()
{
  int i;
  bool b;
  _3 _3_;
  _9 _9_;

  //Q1.
  std::cout << "sizeof(_3_) = " <<sizeof(_3_) << '\n';
  std::cout << "sizeof(_9_) = " <<sizeof(_9_) << '\n';

  //Q2.
  //serialize(_3_).toto; // equintarr<_3> {aka struct std::array<short unsigned int, 1>}   -> OK
  //serialize(_9_).toto;   // equintarr<_9> {aka struct std::array<short unsigned int, 4>} -> OK
  std::cout << "sizeof(serialize(_3_)) = " <<sizeof(serialize(_3_)) << '\n';
  std::cout << "sizeof(serialize(_9_)) = " <<sizeof(serialize(_9_)) << '\n';

  //Q3.
  //std::cout << "sizeof(deserialize(serialize(_3_))) = " <<sizeof(deserialize(serialize(_3_))) << '\n';
  //std::cout << "sizeof(deserialize(serialize(_9_))) = " <<sizeof(deserialize(serialize(_9_))) << '\n';
  return 0;
}
