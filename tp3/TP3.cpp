/*
Thibault Delorme, 20191212
TP3 metaprog
g++ TP3.cpp -std=c++17 -o exe && exe
*/

#include <type_traits>
#include <iostream>


template<typename... Ts>
struct list
{};

template<typename... Ts>
struct box
{};

template<typename L, typename... T> struct push_front;
template<typename L, typename T> struct push_back;
template<typename L> struct front;
template<typename L> struct back;

template<typename L> struct size;
template<template<class> class Func, typename L> struct transform;
template<template<class...> class Box, typename L> struct wrap;

template<template<class...> class List, typename... Ts>
struct size<List<Ts...>>
: std::integral_constant<std::size_t, sizeof...(Ts)>
{};

template<template<class...> class List, typename... Ts,template<class> class Func>
struct transform<Func, List<Ts...>>
{
  using type = List< typename Func<Ts>::type... >;
};

template<template<class...> class List, typename... Ts, template<class...> class Box>
struct wrap<Box, List<Ts...>>
{
  using type = Box< Ts... >;
};

template< template<class...> class List
, typename... Ts, typename T
>
struct push_front<List<Ts...>,T>
{
  using type = List< T, Ts... >;
};

template< template<class...> class List, typename... Ts
, typename T
>
struct push_back<List<Ts...>,T>
{
  using type = List< Ts..., T >;
};

template< template<class...> class List, typename... Ts
, typename First
>
struct front<List<First, Ts...>>
{
  using type = First;
};

struct no_such_type_ {};

template< template<class...> class List>
struct front<List<>>
{
  static_assert(sizeof(List<>) == 0, "Can't access front of empty list");
  using type = no_such_type_;
};

template< template<class...> class List, typename... Ts
, typename First
>
struct back<List<First,Ts...>>
: back<List<Ts...>>
{};

template< template<class...> class List
, typename First
>
struct back<List<First>>
{
  using type = First;
};

template< template<class...> class List>
struct back<List<>>
{
  static_assert(sizeof(List<>) == 0, "Can't access back of empty list");
  using type = no_such_type_;
};

//Q2.

//forward declaration sinon ca passe pas
template<typename L, template<class> class Predicat> struct count;

template<template<class...> class List, template<class> class Predicat>
struct count<List<>,Predicat>
{
  constexpr static int value = 0; //const doit suffire?
};

template<template<class...> class List, template<class> class Predicat, typename T, typename... Ts>
struct count<List<T, Ts...>,Predicat>
{
  constexpr static int value = Predicat<T>::value + count<List<Ts...>,Predicat>::value;
};

//Q3.
template<typename List> struct flatten;

//si on a une liste vide, on retourne une liste vide
template<template<class...> class List>
struct flatten<List<>>
{
  using type = List<>;
};

//on push le premier de la liste
template<template<class...> class List, typename T, typename... Ts>
struct flatten<List<T, Ts...>>
{
  using type = typename push_front<typename flatten<List<Ts...>>::type,T>::type;
};

//la tete de la liste est une liste, on boucle sur elle
template<template<class...> class List, typename... Tss, typename... Ts>
struct flatten<List<List<Tss...>, Ts...>>
{
  using type = typename flatten<List<Tss..., Ts...>>::type;
};

//Q4
template<class... List>  struct merge;

template<template<class...> class List>
struct merge<List<>>
{
  using type = List<>;
};

template<template<class...> class List, typename... T>
struct merge<List<T...>>
{
  using type = List<T...>;
};

template<template<class...> class List, typename... T, typename... Ts>
struct merge<List<T...>,Ts...>
{
  using type = typename push_front<typename merge<Ts...>::type,T...>::type;
};

template<template<class...> class List, typename... T, typename... Ts>
struct merge<List<List<T...>>,Ts...>
{
  using type = typename merge<T...,Ts...>::type;
};

//Q5.
template<typename List, template<class> class Predicat> struct filter;

template<template<class...> class List, template<class> class Predicat>
struct filter<List<>, Predicat>
{
  using type = List<>;
};

template<template<class...> class List, typename T, typename... Ts, template<class> class Predicat>
struct filter<List<T, Ts...>, Predicat>
{
  using type = typename std::conditional<
    Predicat<T>::value,
      typename push_front<typename filter<List<Ts...>, Predicat>::type,T>::type,
      typename filter<List<Ts...>, Predicat>::type
  >::type;
};

//Q6.
template<typename List, template<class> class Predicat> struct partition;
template<template<class> class Predicat, class... List>  struct partition_;


//add 2 empty list at front of List
/*template<template<class...> class List, typename... Ts, template<class> class Predicat>
struct partition<List<Ts...>, Predicat>
{
  using type = typename partition_<Predicat, typename push_front<typename push_front<List<>, list<>>::type, list<>>::type>::type;
};


template<template<class...> class ListOK, template<class...> class ListKO, template<class...> class List, template<class> class Predicat, typename... Ts , typename T>
struct partition_<Predicat, ListOK<>, ListKO<>, List<T, Ts...>>
{
  using type = typename partition_<
    Predicat,
    typename ListOK::type,
    typename ListKO::type,
    typename List<Ts...>
  >::type;
}; MARCHE PAS POUR DES RAISON INCONNUES ?!?*/

template<typename List, template<class> class Predicate> struct partition;

//init
template<template<class...> class List, template<class> class Predicate>
struct partition<List<>,Predicate>
{
  using type = List<List<>,List<>>;
};

template<template<class...> class List, typename T, typename... Ts, template<class> class Predicate>
struct partition<List<T,Ts...>,Predicate>
{
  using listOK = typename front<typename partition<List<Ts...>,Predicate>::type>::type;
  using listKO = typename back<typename partition<List<Ts...>,Predicate>::type>::type;
  using type = typename std::conditional<
    Predicate<T>::value,
    list<typename push_front<listOK,T>::type,listKO>,
    list<listOK,typename push_front<listKO,T>::type>
  >::type; //on push dans la bonne liste en fonction du predicate
};

int main()
{
  //  Q1 - Vérifiez le comportement des meta-fonctions écrites ci dessus en utilisant static_assert
  //       et std::is_same pour comparer leur resultats au resultat attendus
  std::cout << "/* Q1 */" << '\n';
  std::cout << "assert list creation..." << '\n';
  using list_1 = list<bool, int, double>;
  static_assert(std::is_same<list_1, list<bool, int, double>>::value);
  std::cout << "\t...OK\n" << '\n';

  std::cout << "assert push_front..." << '\n';
  using list_2 = push_front<list_1, float>::type;
  static_assert(std::is_same<list_2, list<float, bool, int, double>>::value);
  std::cout << "\t...OK\n" << '\n';

  std::cout << "assert push_back..." << '\n';
  using list_3 = push_back<list_1, float>::type;
  static_assert(std::is_same<list_3, list<bool, int, double, float>>::value);
  std::cout << "\t...OK\n" << '\n';

  std::cout << "assert front..." << '\n';
  using front_ = front<list_1>::type;
  static_assert(std::is_same<front_, bool>::value);
  std::cout << "\t...OK\n" << '\n';

  std::cout << "assert back..." << '\n';
  using back_ = back<list_1>::type;
  static_assert(std::is_same<back_, double>::value);
  std::cout << "\t...OK\n" << '\n';

  std::cout << "assert size..." << '\n';
  static_assert(size<list_1>::value == 3);
  static_assert(size<list_2>::value == 4);
  static_assert(size<list_3>::value == 4);
  std::cout << "\t...OK\n" << '\n';

  std::cout << "assert transform..." << '\n';
  using transform_b = transform<back, list<list<bool, int>, list<bool, int>, list<bool, int>>>::type;
  using transform_f = transform<front, list<list<bool, int>, list<bool, int>, list<bool, int>>>::type;
  static_assert(std::is_same<transform_b, list<int, int, int>>::value);
  static_assert(std::is_same<transform_f, list<bool, bool, bool>>::value);
  std::cout << "\t...OK\n" << '\n';

  std::cout << "assert wrap..." << '\n';
  using box_1 = wrap<box, list_1>::type;
  using box_2 = wrap<box, transform_b>::type;
  static_assert(std::is_same<box_1, box<bool, int, double>>::value);
  static_assert(std::is_same<box_2, box<int, int, int>>::value);
  std::cout << "\t...OK\n" << '\n';


  //  Q2 - Implémentez et testez une meta-fonction count<List,Predicate> qui renvoit une constante
  //       entière qui s'évalue au nombre de types T présents dans List tels que Predicate<T>::value
  //       soit égal à true. Pouvez vous estimer la complexité de son temps de compialtion ?
  //  Exemple:
  //  constexpr int s = count< list<int, float, char>, std::is_integral>::value
  //  Ici s devrait valoir 2
  std::cout << "/* Q2 */" << '\n';
  std::cout << "assert count..." << '\n';
  static_assert(count< list<int>, std::is_integral>::value == 1);
  static_assert(count< list<float>, std::is_integral>::value == 0);
  static_assert(count< list<int, float, char>, std::is_integral>::value == 2);
  std::cout << "\t...OK\n" << '\n';


  //Q3 - Implémentez une meta-fonction flatten qui prend en parametre une
  //liste de type et
  //      qui renvoit une liste de type dans laquelle toutes les listes
  //internes ont été aplaties:
  //Exemple:
  //using f = flatten<list<int, float, list<double, list<char>>>>::type
  //le type f obtenu devrait etre de la forme list<int,float,double,char>
  std::cout << "/* Q3 */" << '\n';
  std::cout << "assert flaten..." << '\n';
  using f = flatten<list<int, float, list<double, list<char>>>>::type;
  static_assert(std::is_same<f, list<int,float,double,char>>::value);
  using f2 = flatten<list<int, list<float, bool, bool>, list<double, list<char>>>>::type;
  static_assert(std::is_same<f2, list<int, float, bool, bool, double, char>>::value);
  std::cout << "\t...OK\n" << '\n';



  /*Q4 - Implémentez une meta-fonction merge qui prend un nombre arbitraire
  de liste de types
  et fusionne ses éléments.
  Exemple:
  using f = merge< list<double>, list<int, float, list<char>>>::type
  le type f obtenu devrait etre de la forme list<double,int,float,list<char>>*/
  std::cout << "/* Q4 */" << '\n';
  std::cout << "assert merge..." << '\n';
  using f3 = merge< list<double>, list<int, float, list<char>>>::type;
  static_assert(std::is_same<f3, list<double,int,float,list<char>>>::value);
  std::cout << "\t...OK\n" << '\n';


  //  Q5 - Implémentez et testez une meta-fonction filter<List,Predicate> qui renvoit la liste
  //       des types T présents dans List tels que Predicate<T>::value soit égal à true. Pouvez
  //       vous estimer la complexité de son temps de compilation ?
  //  Exemple:
  //  using f = filter< list<int, float, char>, std::is_integral>::type
  //  le type f obtenu devrait etre de la forme list<int,char>
  std::cout << "/* Q5 */" << '\n';
  std::cout << "assert filter..." << '\n';
  using f4 = filter< list<int, float, char>, std::is_integral>::type;
  static_assert(std::is_same<f4, list<int, char>>::value);
  std::cout << "\t...OK\n" << '\n';


  //  Q6 - Implémentez une meta-fonction partition<List,Predicate> qui renvoit une liste de liste
  //       contenant en premier lieu la liste des types T verifiant Predicate<T>::value == true
  //       et en deuxieme lieu la liste des types T vérifiant Predicate<T>::value == false. Pouvez
  //       vous estimer la complexité de son temps de compilation ?
  std::cout << "/* Q6 */" << '\n';
  std::cout << "assert partition..." << '\n';
  using f5 = partition< list<int, float, char>, std::is_integral>::type;
  static_assert(std::is_same<f5, list<list<int, char>,list<float>>>::value);
  std::cout << "\t...OK\n" << '\n';


  //  Q7 - En vous inspirant de l'algorithme de quick-sort (https://www.geeksforgeeks.org/quick-sort/)
  //       , en vous aidant des metafonctions précédentes et de toutes autres que vous jugerez utile,
  //       implémentez une metafonction sort<List,Relation> qui trie les types de List en utilisant
  //       le predicat Relation. Pour vous aider, commencer par implémenter des meta-fonctions
  //       mini_sort qui trient un nombre fixe et faible (2,4 ou 8) de types manuellement et partez
  //       de ces implémentations pour reconstruire un tri arbitraire.

  return 0;
}
